# springboot-mybatis

#### 项目介绍
利用mybatis-generator自动生成代码：命令行、eclipse插件、maven插件。此demo是利用maven插件的方式

#### 关键点

1. pom.xml文件中configurationFiles引入自定义配置的generatorConfig.xml路径要正确从src开始
2. generatorConfig.xml中classPathEntry location的绝对路径要用斜杠(而不是反斜杠)demo中用的是本地仓库的mysql驱动路径要正确
3. 生成model、mapper和xml的targetPackage包路径和targetProject项目路径，项目路径耽误我很多时间，路径要从src下的路径

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)